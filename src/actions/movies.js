import { useDispatch } from 'react-redux'
import movies from '../data/post.json'
import axios from '../services/axios'
import service from '../services'

export const LIST_MOVIES = 'LIST_MOVIES'

export const displayMovies = movies => ({
  type: LIST_MOVIES,
  payload: movies
})

export const getNewMovies = movieFavorite => dispatch => {
  axios
    .get(`${service.fetchNewMovies}`)
    .then(res => {
      dispatch(displayMovies(res.data.results))
    })
    .catch(err => err)
}
