import { useDispatch } from 'react-redux'

export const ADD_LIBRARY = 'ADD_LIBRARY'
export const REMOVE_LIBRARY = 'REMOVE_LIBRARY'
export const ADD_SEEN = 'ADD_SEEN'
export const REMOVE_SEEN = 'REMOVE_SEEN'

export const addLibrary = movie => ({
  type: ADD_LIBRARY,
  payload: {
    movie: movie
  }
})

export const removeLibrary = movie => ({
  type: REMOVE_LIBRARY,
  payload: {
    movie: movie.id
  }
})

export const addSeen = movie => ({
  type: ADD_SEEN,
  payload: {
    movie: movie
  }
})

export const removeSeen = movie => ({
  type: REMOVE_SEEN,
  payload: {
    movie: movie.id
  }
})
