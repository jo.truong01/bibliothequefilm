import styled from 'styled-components'

const Container = styled.div`
  width: 70%;
  margin: 0 auto;
  .input-field {
    text-align: right;
    input {
      width: 70%;
    }
  }
  ${props => {
    if (props.detail) {
      return `
      margin: 50px auto 0 auto;
        @media screen and (max-width: 600px) {
          width: 90%;
        }
      `
    } else if (props.Explore) {
      return `
      width: 80%;
      margin: 50px auto;
      `
    }
  }}
`

export default Container
