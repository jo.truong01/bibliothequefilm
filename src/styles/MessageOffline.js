import styled from 'styled-components'

const MessageOffline = styled.div`
  padding: 55px 0;
  text-align: center;
  .message {
    font-size: 30px;
    font-weight: 700;
    @media screen and (max-width: 600px) {
      font-size: 18px;
    }
  }
`
export default MessageOffline
