import styled from 'styled-components'

const Slide = styled.div`
  width: 80%;
  margin: 0 auto;
  .swiper__library {
    height: 70vh;
    @media screen and (max-width: 750px) {
      height: 50vh;
    }
    @media screen and (max-width: 550px) {
      height: 30vh;
    }
  }
  .swiper__image {
    position: relative;

    .back__image {
      background-image: ${props => props.url};
      background-size: cover;
      height: 70vh;
      background-position: top center;
      @media screen and (max-width: 750px) {
        height: 50vh;
      }
      @media screen and (max-width: 550px) {
        height: 30vh;
      }
    }
  }
`

export default Slide
