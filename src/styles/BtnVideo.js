import styled from 'styled-components'

const BtnVideo = styled.div`
  transition: all 0.3s ease-in-out;
  cursor: pointer;
  img {
    width: 60px;
    height: 60px;
  }
  &:hover {
    opacity: 0.8;
  }
`

export default BtnVideo
