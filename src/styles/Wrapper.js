import styled from 'styled-components'

const Wrapper = styled.div`
  .detail__down {
    padding: 30px 0;
    .detail__first {
      display: flex;
      @media screen and (max-width: 800px) {
        flex-direction: column-reverse;
      }
    }
    .detail__last {
      padding: 50px 0 0 0;
    }
    .image__poster {
      width: 200px;
      margin-right: 25px;
      img {
        width: 200px;
      }
      @media screen and (max-width: 800px) {
        display: none;
      }
    }
  }

  .detail {
    width: 100%;
    .detail__title {
      margin-bottom: 25px;
    }
    .detail__description {
      line-height: 25px;
      font-size: 15px;
      width: 80%;
      @media screen and (max-width: 800px) {
        width: 100%;
      }
    }
    .detail__buttons {
      display: flex;
    }
    p {
      margin: 0;
      font-weight: 700px;
    }
    .detail__genres {
      margin: 0;
      ul {
        display: inline-block;
        list-style: none;
        padding: 0;
        a {
          color: #000;
          text-decoration: none;
          &:hover {
            text-decoration: underline;
            color: #8f3338ef;
          }
        }
        li {
          padding: 0;
        }
      }
    }
  }
`

export default Wrapper
