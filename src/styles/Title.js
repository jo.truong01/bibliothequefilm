import styled from 'styled-components'

const Title = styled.h2`
  margin: 0;
  font-size: 27px;
  @media screen and (max-width: 550px) {
    font-size: 20px;

  ${props => {
    if (props.account) {
      return `
        width:70%;
        margin: 0 auto;
      `
    }
  }}
`

export default Title
