import { createGlobalStyle } from 'styled-components'

export const GlobalStyles = createGlobalStyle`
  body {
    background-color: ${({ theme }) => theme.body};
    color: ${({ theme }) => theme.text};
    font-family: 'Roboto', sans-serif;
    transition: all .3s linear;
  }
  p {
    line-height: 1.4rem;
  }
  .btn-primary {
    background: ${({ theme }) => theme.primary};
    color: ${({ theme }) => theme.body};
    padding: 0.5rem 1.5rem;
    font-size: 1rem;
    border-radius: 1rem;
    cursor: pointer;
    outline: none;
    border: none;
    transition: all .3s linear;
  }
`

export const lightTheme = {
  name: 'light',
  body: '#fff',
  text: '#121212',
  primary: '#6200ee'
}

export const darkTheme = {
  name: 'dark',
  body: '#121212',
  text: '#fff',
  primary: '#bb86fc'
}
