import styled from 'styled-components'

const Card = styled.div`
  margin: 50px 0;
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(210px, 1fr));
  gap: 2rem;
  .explore__content {
    transform: scale(1);
    transition: all 0.4s;
    &:hover {
      transform: scale(1.07);
      z-index: 1;
    }
    &:hover .btn {
      display: block;
    }
    &:hover .icon {
      display: block;
    }
  }
  .explore__poster {
    align-self: flex-start;
    &:hover {
      transform: scale(1.08);
    }
  }
  .contents {
    position: relative;
    z-index: 1;
    .icon {
      display: none;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      transition: all 0.4s;
      img {
        width: 60px;
        border-radius: 50%;
        padding: 5px;
      }
    }
    .iconb {
      img {
        width: 45px;
      }
    }
  }
  .ishide {
    display: none;
  }
  .btn {
    display: none;
    position: absolute;
  }
`

export default Card
