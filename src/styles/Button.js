import styled from 'styled-components'

const Button = styled.button`
  border: none;
  position: relative;
  cursor: pointer;
  outline: none;
  transition: all 0.3s ease-in-out;
  &:hover {
    opacity: 0.8;
  }
  @media screen and (max-width: 800px) {
    padding: 10px 20px;
  }
  ${props => {
    if (props.detail) {
      return `
      color: inherit;
      background-color: #fff;
      `
    } else if (props.libray) {
      return `
      border-radius: 5px;
      padding: 10px 20px;
      margin-right: 15px;
      font-size: 17px;
      background-color: #b9c5d1d5;
      &:hover{
        background-color:  #d5e0ecc0;
      }
      
      `
    } else if (props.delete) {
      return `
      background-color: #b8393fc0;
      `
    }
  }}
`

export default Button
