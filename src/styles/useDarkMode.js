import { useState, useEffect } from 'react'
import { darkTheme, lightTheme } from './globalStyles'

export const useDarkMode = () => {
  const [theme, setTheme] = useState(darkTheme)

  const setMode = (mode, selectedTheme) => {
    window.localStorage.setItem('theme', JSON.stringify(selectedTheme))
    setTheme(selectedTheme)
  }

  const toggleTheme = () => {
    theme.name === 'dark'
      ? setMode('light', lightTheme)
      : setMode('dark', darkTheme)
  }

  useEffect(() => {
    const localTheme = JSON.parse(window.localStorage.getItem('theme'))
    localTheme ? setTheme(localTheme) : setMode('dark', darkTheme)
  }, [])

  return [theme, toggleTheme]
}
