import styled from 'styled-components'

const NavBars = styled.div`
  padding-top: 10px;
  display: flex;
  @media screen and (max-width: 530px) {
    padding-top: 20px;
  }
  .logo {
    margin-left: 50px;
    @media screen and (max-width: 530px) {
      margin-left: 10px;
    }
    img {
      width: 120px;
      @media screen and (max-width: 850px) {
        width: 110px;
      }
      @media screen and (max-width: 530px) {
        width: 90px;
      }
    }
  }
  .link__menu {
    padding-top: 10px;
    margin-left: 15px;
    align-self: center;
    @media screen and (max-width: 850px) {
      margin-left: 12px;
    }
    @media screen and (max-width: 530px) {
      margin-left: 10px;
    }
    .item__menu {
      list-style-type: none;
      margin: 0;
      padding: 0;
      li {
        .parcourir {
          display: none;
        }
        ul {
          padding: 0;
          list-style-type: none;
          display: block;
          li {
            display: inline-block;
            a {
              text-decoration: none;
              padding: 0 3px;
              margin: 0 3px;
              font-size: 18px;
              font-weight: 500;
              color: inherit;
              transition: all 0.4s;
              @media screen and (max-width: 910px) {
                font-size: 15px;
              }
              @media screen and (max-width: 800px) {
                font-size: 13px;
                margin: 0 1px;
                padding: 0 1px;
              }
              @media screen and (max-width: 745px) {
                font-size: 12px;
              }
              &:hover {
                color: #4071ad;
              }
            }
          }
        }
        @keyframes animate__block {
          0% {
            display: block;
          }
          70% {
            display: block;
          }
          100% {
            display: none;
          }
        }
        @media screen and (max-width: 700px) {
          .parcourir {
            display: block;
            font-weight: 700;
            position: relative;
            outline: none;
            cursor: pointer;
            z-index: 9;

            &:focus + .item__ul {
              display: block;
            }

            &::after {
              content: '';
              position: absolute;
              right: -16px;
              top: 4px;
              border-top: 8px solid #4071ad;
              border-left: 5px solid transparent;
              outline: none;
              border-right: 5px solid transparent;
              z-index: 999999;
            }
          }
          .item__ul {
            display: none !important;
            position: absolute;
            z-index: 10;
            top: 90px;
            left: 0;
            right: 0;
            border-bottom: 1px solid #323f4ead;
            &.show__menu {
              display: block !important;
            }

            li {
              display: block;
              padding: 15px 10px;
              background-color: #252e39;
              color: #e4ecf5;
              border-bottom: 1px solid #323f4ead;
            }
          }
        }
        @media screen and (max-width: 530px) {
          .parcourir {
            font-size: 13px;
          }
        }
      }
    }
  }
`
export default NavBars
