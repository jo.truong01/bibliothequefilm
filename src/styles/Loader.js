import styled, { keyframes } from 'styled-components'

const placeHolderShimmer = keyframes`
 0%{
       background-position: -486px 0px
  }
  100%{
       background-position: 486px 0px
  }
`

const LoaderMovie = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  height: 100vh;
  z-index: 99;
  .movie-card-loader {
    height: 157px;
    background-color: #ffffff;
    padding: 20px;

    .shimmer-block {
      width: 100%;
      height: 100%;

      animation-duration: 2s;
      animation-fill-mode: forwards;
      animation-iteration-count: infinite;
      animation-name: ${placeHolderShimmer};
      animation-timing-function: linear;
      background: #f6f7f8;
      background: linear-gradient(
        to right,
        #eeeeee 8%,
        #dddddd 18%,
        #eeeeee 33%
      );

      .right-column {
        position: relative;
        float: right;
        width: 140px;
        height: 100%;
      }

      .left-column {
        float: left;
        width: 102px;
        // height: 100%;
      }

      .content-column {
        position: relative;
        margin: 0 120px;
        height: 100%;
      }

      .shimmer-mask {
        position: absolute;
        background-color: #ffffff;

        &.poster-right {
          left: -18px;
          height: 100%;
          width: 18px;
        }

        &.title-bottom {
          top: 35px;
          height: 18px;
          width: 100%;
        }

        &.language-bottom {
          top: 77px;
          height: 8px;
          width: 100%;
        }

        &.genre-bottom {
          top: 103px;
          height: 16px;
          width: 100%;
        }

        &.title-right {
          right: 0;
          top: 0;
          width: 25%;
          height: 35px;
        }

        &.language-right {
          right: 0;
          top: 53px;
          width: 65%;
          height: 24px;
        }

        &.genre-right {
          right: 0;
          top: 79px;
          width: 50%;
          height: 24px;
        }

        &.tags-right {
          right: 0;
          top: 119px;
          width: 94%;
          height: 37px;
        }

        &.rating-box {
          top: 0;
          width: 100%;
          height: 50px;
          background-color: transparent;
          border-top: 15px solid #ffffff;
          border-right: 32px solid #ffffff;
          border-bottom: 5px solid #ffffff;
          border-left: 50px solid #ffffff;
          box-sizing: border-box;
        }

        &.votes-box {
          top: 50px;
          width: 100%;
          height: 35px;
          background-color: transparent;
          border-top: 5px solid #ffffff;
          border-right: 12px solid #ffffff;
          border-bottom: 10px solid #ffffff;
          border-left: 30px solid #ffffff;
          box-sizing: border-box;
        }

        &.book-button-box {
          top: 80px;
          width: 100%;
          height: 50px;
          background-color: transparent;
          border-bottom: 40px solid #ffffff;
          // box-sizing: border-box;
        }
      }
    }
  }
`

export default LoaderMovie
