import styled from 'styled-components'

const MyLibraryStyled = styled.div`
  margin: 50px 0;

  .title__mylibrary{
    width: 80%; 
    margin: 0 auto;
  }

  .breadcrumb {
    width: 80%;
    text-align: center;
    margin: 50px auto;
    ul {
      margin: 0;
      padding: 0;
      list-style-type: none;
      display: inline-block;
      @media screen and (max-width: 470px) {
        display: flex;
        justify-content: space-between;
      }

      li {
        display: inline;
        transition: all 0.3s;
        cursor: pointer;
        margin: 0 10px;
          @media screen and (max-width: 470px) {
            display: flex;
            justify-content: space-between;
          }
          .span__breadcrumb {
            font-size: 30px;
            text-decoration: underline;
          }
          .span__decoration{
              opacity: 0.6;
              font-size: 30px;
          }
          @media screen and (max-width: 700px) {
            .span__breadcrumb {
              font-size: 20px;
            }
            .span__decoration{
              font-size: 20px;
            }
          }
          @media screen and (max-width: 470px) {
            .span__breadcrumb {
              font-size: 16px;
            }
            .span__decoration{
                font-size: 16px;
              }
            }
        }
      }
    }
  }
`
export default MyLibraryStyled
