import styled from 'styled-components'

const DeleteBtn = styled.button`
  border: none;
  position: relative;
  cursor: pointer;
  outline: none;
  border-radius: 5px;
  padding: 10px 25px;
  margin-right: 15px;
  font-size: 20px;
  transition: all 0.3s ease-in-out;
  background-color: #be2b32;
  color: #fff;
  &:hover {
    background-color: rgba(191, 43, 51, 0.88);
  }
  @media screen and (max-width: 800px) {
    padding: 9px 20px;
  }
`
export default DeleteBtn
