import styled from 'styled-components'

const Rows = styled.div`
  width: 88%;
  margin: 30px auto;
  .row__title {
    display: flex;
    align-self: center;
    span {
      align-self: center;
      cursor: pointer;
      a {
        text-decoration: none;
        font-size: 18px;
        color: inherit;
        margin-left: 20px;
        img {
          width: 28px;
        }
      }
      &:hover {
        text-decoration: underline;
      }
    }
  }
  img {
    width: 100%;
    margin: 0;
    padding: 0;
  }
  .card__poster {
    transform: scale(1);
    transition: all 0.3s;
    padding: 30px 0 20px 0;
    &:hover {
      transform: scale(1.05);
      z-index: 100;
    }
    &:hover .icon {
      transition: all 0.3s;
      opacity: 1;
    }
  }

  .backdrop {
    padding: 10px 0;
  }
  .card__backdrop {
    transform: scale(1);
    transition: all 0.3s;
    padding: 0px 0;
    &:hover {
      transform: scale(1.3);
      z-index: 100;
    }
    &:hover .icon {
      transition: all 0.3s;
      opacity: 1;
    }
  }
  .cardb {
    position: relative;
    .icon {
      position: absolute;
      opacity: 0;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      transition: all 0.3s;
      img {
        cursor: pointer;
        width: 40px;
        border-radius: 50%;
        padding: 10px;
        @media screen and (max-width: 550px) {
          width: 17px;
          padding: 8px;
        }
      }
    }
  }
  .cardp {
    position: relative;
    .icon {
      opacity: 0;
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      transition: all 0.3s;

      img {
        cursor: pointer;
        width: 60px;
        border-radius: 50%;
        padding: 10px;
        @media screen and (max-width: 550px) {
          width: 25px;
          padding: 10px;
        }
      }
    }
  }
  .ishide {
    display: none;
  }

  .swiper-button-prev {
    top: 50% !important;
    transform: translateY(-50%);
    &::after {
      font-size: 40px !important;
    }
  }
  .swiper-button-next {
    top: 50% !important;
    transform: translateY(-50%);
    &::after {
      font-size: 40px !important;
    }
  }
  .btn__prev {
    &::after {
      font-size: 30px !important;
    }
  }
  .btn__next {
    &::after {
      font-size: 30px !important;
    }
  }
`

export default Rows
