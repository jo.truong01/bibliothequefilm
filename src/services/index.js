const API_KEY = process.env.REACT_APP_API_KEY

const today = new Date(),
  date =
    today.getFullYear() +
    '-' +
    ('0' + (today.getMonth() + 1)).slice(-2) +
    '-' +
    ('0' + today.getDate()).slice(-2)

const Services = {
  // Pour le home sans id
  fetchTvOriginals: `/discover/tv?api_key=${API_KEY}&with_networks=213`, //https://api.themoviedb.org/3/discover/tv?api_key=0fab4c543a68eb2d7012be51e8411943&with_networks=213
  fetchTrending: `/trending/all/week?api_key=${API_KEY}`, // https://api.themoviedb.org/3/trending/all/week?api_key=0fab4c543a68eb2d7012be51e8411943
  fetchAction: `/discover/movie?api_key=${API_KEY}&with_genres=28`, //https://api.themoviedb.org/3/discover/movie?api_key=0fab4c543a68eb2d7012be51e8411943&page=1&with_genres=28
  fetchAdventure: `/discover/movie?api_key=${API_KEY}&with_genres=12`, // https://api.themoviedb.org/3/discover/movie?api_key=0fab4c543a68eb2d7012be51e8411943&page=1&with_genres=12
  fetchAnimation: `/discover/movie?api_key=${API_KEY}&with_genres=16`, //https://api.themoviedb.org/3/discover/movie?api_key=0fab4c543a68eb2d7012be51e8411943&page=1&with_genres=16
  fetchComedy: `/discover/movie?api_key=${API_KEY}&with_genres=35`, //https://api.themoviedb.org/3/discover/movie?api_key=0fab4c543a68eb2d7012be51e8411943&page=1&with_genres=35
  fetchDrama: `/discover/movie?api_key=${API_KEY}&with_genres=80`, //https://api.themoviedb.org/3/discover/movie?api_key=0fab4c543a68eb2d7012be51e8411943&page=1&with_genres=80
  fetchDocumentary: `/discover/movie?api_key=${API_KEY}&with_genres=99`, //https://api.themoviedb.org/3/discover/movie?api_key=0fab4c543a68eb2d7012be51e8411943&page=1&with_genres=99
  fetchDrime: `/discover/movie?api_key=${API_KEY}&with_genres=18`, //https://api.themoviedb.org/3/discover/movie?api_key=0fab4c543a68eb2d7012be51e8411943&page=1&with_genres=18
  fetchFamily: `/discover/movie?api_key=${API_KEY}&with_genres=10751`, //https://api.themoviedb.org/3/discover/movie?api_key=0fab4c543a68eb2d7012be51e8411943&page=1&with_genres=10751
  fetchFantasy: `/discover/movie?api_key=${API_KEY}&with_genres=14`, //https://api.themoviedb.org/3/discover/movie?api_key=0fab4c543a68eb2d7012be51e8411943&page=1&with_genres=14
  fetchHistory: `/discover/movie?api_key=${API_KEY}&with_genres=36`, // https://api.themoviedb.org/3/discover/movie?api_key=0fab4c543a68eb2d7012be51e8411943&page=1&with_genres=36
  fetchScienceFiction: `/discover/movie?api_key=${API_KEY}&with_genres=878`, //https://api.themoviedb.org/3/discover/movie?api_key=0fab4c543a68eb2d7012be51e8411943&page=1&with_genres=878
  fetchTvMovie: `/discover/movie?api_key=${API_KEY}&with_genres=10770`, // https://api.themoviedb.org/3/discover/movie?api_key=0fab4c543a68eb2d7012be51e8411943&page=1&with_genres=10770
  fetchPopular: `/movie/popular?api_key=${API_KEY}`, // https://api.themoviedb.org/3/movie/popular?api_key=0fab4c543a68eb2d7012be51e8411943&page=1

  // Avec id
  fetchCasting: `/movie/credits?api_key=${API_KEY}`, //https://api.themoviedb.org/3/movie/550/credits?api_key=0fab4c543a68eb2d7012be51e8411943
  fetchVideos: `/videos?api_key=${API_KEY}`, //https://api.themoviedb.org/3/movie/550/videos?api_key=0fab4c543a68eb2d7012be51e8411943
  fetchSimilar: `/movie/similar?api_key=${API_KEY}`, //https://api.themoviedb.org/3/movie/550/similar?api_key=0fab4c543a68eb2d7012be51e8411943&page=1
  fetchRecommandation: `/movie/recommendations?api_key=${API_KEY}`, //https://api.themoviedb.org/3/movie/550/recommendations?api_key=0fab4c543a68eb2d7012be51e8411943&page=1

  fetchMovieById: `?api_key=${API_KEY}`, //https://api.themoviedb.org/3/movie/550?api_key=0fab4c543a68eb2d7012be51e8411943
  fetchNewMovies: `/discover/movie?api_key=${API_KEY}&language=en-US&sort_by=primary_release_date.desc&include_adult=false&include_video=false&page=1&primary_release_date.lte=${date}`, //language=en-US&sort_by=primary_release_date.desc&include_adult=false&include_video=false&page=1&primary_release_date.lte=2020-11-27

  // Par la meme nomenclature
  fetchSearch: `/search?api_key=${API_KEY}`, //https://api.themoviedb.org/3/search/movie?api_key=0fab4c543a68eb2d7012be51e8411943&query=marvel&page=1
  fetchGenres: `/genre/list?api_key=${API_KEY}` //https://api.themoviedb.org/3/genre/movie/list?api_key=0fab4c543a68eb2d7012be51e8411943 ,
}

export default Services
