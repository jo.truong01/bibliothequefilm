import Axios from 'axios'

const Instance = Axios.create({
  baseURL: 'https://api.themoviedb.org/3'
})

export default Instance
