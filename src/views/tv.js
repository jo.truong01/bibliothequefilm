import React, { useState, useEffect } from 'react'
import { Link, useParams } from 'react-router-dom'
import PropTypes from 'prop-types'
import axios from 'axios'
import {
  addLibrary,
  addSeen,
  removeLibrary,
  removeSeen
} from '../actions/library'
import { connect } from 'react-redux'
import styled from 'styled-components'
import YouTube from 'react-youtube'
import movieTraider from 'movie-trailer'
// components
import BtnLibrarySeen from '../components/btnLibrary/BtnLibrarySeen'
import BtnLibraryNotSeen from '../components/btnLibrary/btnLibraryNotSeen'
import BtnVideoPlay from '../components/BtnVideoPlay'
import Loader from '../components/loader'
import Recommended from '../components/Recommended'
import BtnFavorite from '../components/BtnFavorite'
// styles
import Container from '../styles/Container'
import Title from '../styles/Title'
import Wrapper from '../styles/Wrapper'
import MessageOffline from '../styles/MessageOffline'
// images
import ImageError from '../assets/error.PNG'

const TV = ({ library, seen }) => {
  const [isLoading, setIsLoading] = useState(true)
  const [movie, setMovie] = useState([])
  const [trailerUlr, setTrailerUlr] = useState('')
  const [errorMessage, setErrorMessage] = useState(false)
  let key = process.env.REACT_APP_API_KEY
  let base_urlImage = process.env.REACT_APP_BASE_URL_IMAGE
  const param = useParams()

  useEffect(() => {
    getDetailTv()
  }, [])

  const opts = {
    height: '500',
    width: '100%',
    playerVars: {
      autoplay: 1
    }
  }

  const handleClick = movie => {
    if (trailerUlr) {
      setTrailerUlr('')
    } else {
      movieTraider(movie?.name || movie?.title || movie?.original_name || '')
        .then(url => {
          const urlParams = new URLSearchParams(new URL(url).search)
          setTrailerUlr(urlParams.get('v'))
        })
        .catch(error => {
          setErrorMessage(true)
        })
    }
  }

  const getDetailTv = () => {
    axios
      .get(`https://api.themoviedb.org/3/tv/${param.id}?api_key=${key}`)
      .then(resp => {
        setMovie(resp.data)
        setIsLoading(false)
      })
      .catch(err => {
        setIsLoading(false)
        console.error('ERROR: ', err)
      })
  }

  if (isLoading) {
    return <Loader />
  }
  const existsInLibrary = movie => {
    return library.find(item => item.id === movie.id)
  }
  const existsInSeen = movie => {
    return seen.find(item => item.id === movie.id)
  }
  const name = () => {
    return param.name[0].toUpperCase() + param.name.slice(1)
  }

  return (
    <Container detail>
      {movie ? (
        <Wrapper>
          {!errorMessage ? (
            <div>
              {trailerUlr ? (
                <div>
                  {trailerUlr && <YouTube videoId={trailerUlr} opts={opts} />}
                </div>
              ) : (
                <ContainerImage
                  url={`url(${base_urlImage}${
                    movie?.backdrop_path || movie?.poster_path
                  })`}
                >
                  <div className='btn__play'>
                    <BtnVideoPlay movie={movie} handleClick={handleClick} />
                  </div>
                </ContainerImage>
              )}
            </div>
          ) : (
            <ContainerImage url={`url(${ImageError})`}>
              <div className='content__message'>
                <MessageOffline>
                  <p className='message'>Pas de detail pour ce film</p>
                </MessageOffline>
              </div>
            </ContainerImage>
          )}

          <div className='detail__down'>
            <div className='detail__first'>
              <div className='image__poster'>
                <img
                  src={`${base_urlImage}${movie?.poster_path}`}
                  className='img-responsive'
                  alt='Image'
                />
              </div>
              <div className='detail'>
                <div
                  className='detail__title'
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <Title>{name()}</Title>
                  <div
                    style={{
                      position: 'relative',
                      top: '-12px',
                      alignSelf: 'center',
                      marginLeft: '25px'
                    }}
                  >
                    <BtnFavorite detail top id={movie.id} />
                  </div>
                </div>

                <div className='detail__description'>{movie.overview}</div>

                <div style={{ marginTop: '20px' }}>
                  <p>
                    <label>Popularité</label>:<span>{movie.popularity} </span>
                  </p>
                  <p className='detail__genres'>
                    Genre :
                    {movie.genres.map(item => (
                      <ul key={item.id}>
                        <Link to={`/explore/${item.name}/${item.id}`}>
                          <li style={{ textDecoration: 'underline' }}>
                            &nbsp;{item.name},
                          </li>
                        </Link>
                      </ul>
                    ))}
                  </p>

                  <p style={{ marginBottom: '15px' }}>
                    Durée épisode : {movie.episode_run_time} min
                  </p>

                  <p style={{ marginBottom: '15px' }}>
                    En cours:
                    {movie.in_production ? (
                      <em> &nbsp; En cours</em>
                    ) : (
                      <em> &nbsp;Terminée</em>
                    )}
                  </p>
                  <p style={{ marginBottom: '15px' }}>
                    Dernier épisode sortie : {movie.last_air_date}
                  </p>
                </div>

                <div
                  className='detail__buttons'
                  style={{ marginBottom: '25px' }}
                >
                  <BtnLibrarySeen
                    existsInSeen={existsInSeen(movie)}
                    removeSeen={removeSeen(movie)}
                    addSeen={addSeen(movie)}
                  />
                  <BtnLibraryNotSeen
                    existsInLibrary={existsInLibrary(movie)}
                    removeLibrary={removeLibrary(movie)}
                    addLibrary={addLibrary(movie)}
                  />
                </div>
              </div>
            </div>
            <div className='detail__last'>
              <Recommended id={movie.id} type='Movie' />
            </div>
          </div>
        </Wrapper>
      ) : (
        <MessageOffline>
          <p className='message'>Pas de detail pour ce film</p>
        </MessageOffline>
      )}
    </Container>
  )
}

TV.propTypes = {
  library: PropTypes.array,
  seen: PropTypes.array
}

const mapStateToProps = state => ({
  library: state.library.movieLibrary,
  seen: state.library.movieSeen
})

export default connect(mapStateToProps)(TV)

const ContainerImage = styled.div`
  background-image: ${props => props.url};
  background-size: cover;
  height: 500px;
  background-position: top center;
  position: relative;
  z-index: 1;
  @media screen and (max-width: 600px) {
    height: 200px;
  }
  .btn__play {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .content__message {
    display: none;
    position: relative;
    top: 60px;
  }
  &:hover .content__message {
    display: block;
  }
`
