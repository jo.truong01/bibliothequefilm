import React from 'react'
import { Link } from 'react-router-dom'

const NoLog = () => {
  return (
    <div>
      <h2>
        Obligé de se connecter pour accéder <Link to='/'>Login</Link>
      </h2>
    </div>
  )
}

export default NoLog
