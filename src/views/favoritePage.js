import PropTypes from 'prop-types'
import React, { useEffect, useState } from 'react'
import { connect, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
// components
import BtnFavorite from '../components/BtnFavorite'
// styles
import Title from '../styles/Title'
import Card from '../styles/Cards'
import Container from '../styles/Container'
// images
import Play from '../assets/play.png'
import MessageOffline from '../styles/MessageOffline'

const FavoritePage = props => {
  const dispatch = useDispatch()
  const [displayRecommend, setDisplay] = useState(false)

  let url_image = process.env.REACT_APP_BASE_URL_IMAGE

  const handleRecommend = () => {
    setDisplay(true)
  }

  return (
    <Container Explore>
      <Title>Les Favoris</Title>
      {props.favoris.length > 0 ? (
        <Card>
          {props.favoris.map(movie => (
            <div key={movie.id} className='explore__content'>
              <Link
                to={
                  movie.name
                    ? `/tv/${movie?.id}/${movie?.name}`
                    : `/movie/${movie?.id}/${movie?.title}`
                }
              >
                <div className='contents'>
                  <div className='icon iconb'>
                    <img src={Play} alt='Image' />
                  </div>
                  <img
                    className='new__movie'
                    src={`${url_image}${
                      movie?.backdrop_path || movie?.poster_path
                    }`}
                  />
                </div>
              </Link>
              <div>
                <BtnFavorite movie={movie}></BtnFavorite>
              </div>
            </div>
          ))}
        </Card>
      ) : (
        <MessageOffline>
          <p className='message'>Pas de favoris</p>
        </MessageOffline>
      )}
    </Container>
  )
}
FavoritePage.propTypes = {
  favoris: PropTypes.array
}

const mapStateToProps = state => ({
  favoris: state.favorite.movieFavorite
})

export default connect(mapStateToProps)(FavoritePage)
