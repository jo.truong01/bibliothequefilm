/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react'
import axios from 'axios'
// components
import PopularMovie from '../components/searchMovie/popularMovie'
import SearchMovie from '../components/searchMovie/searchMovie'
// styles
import Title from '../styles/Title'
import Container from '../styles/Container'

const SearchArea = () => {
  const [searchTerm, setSearchTerm] = useState([])
  const [formSearch, setFormSearch] = useState()
  const [movieForm, setMovieForm] = useState([])
  const apiKey = process.env.REACT_APP_API_KEY

  useEffect(() => {
    handleMovie()
  }, [])

  const handleMovie = () => {
    axios
      .get(`https://api.themoviedb.org/3/movie/popular?api_key=${apiKey}`)
      .then(res => {
        setMovieForm(res.data.results)
      })
  }

  const handleSubmit = e => {
    e.preventDefault()
    axios
      .get(
        `https://api.themoviedb.org/3/search/movie?api_key=${apiKey}&query=${formSearch}`
      )
      .then(res => {
        setSearchTerm(res.data.results)
      })
  }

  const handleChange = e => {
    setFormSearch(e.target.value)
  }

  return (
    <Container Explore>
      <div
        className='row'
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          margin: '55px 0'
        }}
      >
        <Title> Recherche </Title>
        <form action='' onSubmit={e => handleSubmit(e)}>
          <div className='input-field'>
            <input
              placeholder='Search movie'
              name='formSearch'
              type='text'
              onChange={handleChange}
            />
          </div>
        </form>
      </div>
      <section className='col offset-s4'>
        <>
          {formSearch ? (
            <SearchMovie searchTerm={searchTerm} />
          ) : (
            <PopularMovie handleMovie={handleMovie} movieForm={movieForm} />
          )}
        </>
      </section>
    </Container>
  )
}
export default SearchArea
