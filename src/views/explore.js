import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import axios from '../services/axios'
// components
import Loader from '../components/loader'
import BtnFavorite from '../components/BtnFavorite'
// styles
import Container from '../styles/Container'
import MessageOffline from '../styles/MessageOffline'
import Title from '../styles/Title'
import Card from '../styles/Cards'
// image
import Play from '../assets/play.png'

const Explore = () => {
  const [genresMovie, setGenresMovie] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  let key = process.env.REACT_APP_API_KEY
  let base_urlImage = process.env.REACT_APP_BASE_URL_IMAGE
  let params = useParams()

  useEffect(() => {
    axios
      .get(
        `https://api.themoviedb.org/3/discover/movie?api_key=${key}&with_genres=${params.id}`
      )
      .then(res => {
        setGenresMovie(res.data.results)
        setIsLoading(false)
      })
      .catch(err => console.error('ERROR : ', err))
  }, [])

  const name = () => {
    return params.name[0].toUpperCase() + params.name.slice(1).replace('-', ' ')
  }

  if (isLoading) {
    return <Loader />
  }

  return (
    <Container Explore>
      <Title> {name()} </Title>

      <div>
        {genresMovie.length > 0 ? (
          <Card>
            {genresMovie.map(movie => (
              <div
                key={movie.id}
                className={movie?.backdrop_path ? 'explore__content' : 'ishide'}
              >
                <Link
                  to={
                    movie.name
                      ? `/tv/${movie.id}/${movie.name}`
                      : `/movie/${movie.id}/${movie.title}`
                  }
                >
                  <div className='contents'>
                    <div className='icon iconb'>
                      <img src={Play} alt='Image' />
                    </div>
                    <img
                      src={`${base_urlImage}${movie?.backdrop_path}`}
                      className='img-responsive'
                      alt={movie?.title || movie?.name}
                    />
                  </div>
                </Link>
                <div>
                  <BtnFavorite id={movie.id}></BtnFavorite>
                </div>
              </div>
            ))}
          </Card>
        ) : (
          <MessageOffline>
            <p className='message'>Pas de films</p>
          </MessageOffline>
        )}
      </div>
    </Container>
  )
}

export default Explore
