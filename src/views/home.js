import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import ReactNotification from 'react-notifications-component'
import { store } from 'react-notifications-component'
import 'react-notifications-component/dist/theme.css'
import Animate from 'animate.css-react'
import 'animate.css'
import service from '../services'
// components
import Banner from '../components/Banner'
import Row from '../components/Row'

const Home = () => {
  useEffect(() => {
    handleOnClickDefault()
  }, [])

  const { t, i18n } = useTranslation()

  const handleOnClickDefault = () => {
    store.addNotification({
      title: 'Warning',
      message: 'Une grande Promotion pour vous',
      type: 'success',
      container: 'top-right',
      insert: 'top',
      animationIn: ['animated', 'fadeIn'],
      animationOut: ['animated', 'fadeOut'],

      dismiss: {
        duration: 3000,
        showIcon: true
      },
      width: 300
    })
  }

  return (
    <div>
      <ReactNotification />
      <Banner />

      <div className='rows'>
        <Row
          title={t('home.televisionoriginal')}
          fetchUrl={service.fetchTvOriginals}
          isPosterRow
          isExplore
        />
        <Row
          title={t('home.tendancesactuelles')}
          fetchUrl={service.fetchTrending}
          isExplore
        />
        <Row
          title={t('home.filmpopulaire')}
          fetchUrl={service.fetchPopular}
          isExplore
        />
        <Row
          title={t('home.actions')}
          fetchUrl={service.fetchAction}
          withGenres={28}
        />
        <Row
          title={t('home.aventure')}
          fetchUrl={service.fetchAdventure}
          withGenres={12}
        />
        <Row
          title={t('home.anime')}
          fetchUrl={service.fetchAnimation}
          withGenres={16}
        />
        <Row
          title={t('home.comedie')}
          fetchUrl={service.fetchComedy}
          withGenres={35}
        />
        <Row
          title={t('home.fantastique')}
          fetchUrl={service.fetchFantasy}
          withGenres={14}
        />
      </div>
    </div>
  )
}

export default Home
