import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { getNewMovies } from '../actions/movies'
// components
import BtnFavorite from '../components/BtnFavorite'
// styles
import Title from '../styles/Title'
import Card from '../styles/Cards'
import Container from '../styles/Container'
// images
import Play from '../assets/play.png'
import MessageOffline from '../styles/MessageOffline'

const NewMovie = () => {
  const dispatch = useDispatch()
  const movies = useSelector(state => state.movies.listMovies)
  let base_urlImage = process.env.REACT_APP_BASE_URL_IMAGE
  useEffect(() => {
    dispatch(getNewMovies())
  }, [])

  return (
    <Container Explore>
      <Title>Nouveautés</Title>
      {movies.length > 0 ? (
        <div>
          <Card>
            {movies.map(movie => (
              <div
                key={movie.id}
                className={movie?.backdrop_path ? 'explore__content' : 'ishide'}
              >
                <Link
                  to={
                    movie.name
                      ? `/tv/${movie.id}/${movie.name}`
                      : `/movie/${movie.id}/${movie.title}`
                  }
                >
                  <div className='contents'>
                    <div className='icon iconb'>
                      <img src={Play} alt='Image' />
                    </div>
                    <img
                      src={`${base_urlImage}${movie?.backdrop_path}`}
                      className='img-responsive new__movie'
                      alt={movie?.title || movie?.name}
                    />
                  </div>
                </Link>
                <div>
                  <BtnFavorite movie={movie}></BtnFavorite>
                </div>
              </div>
            ))}
          </Card>
        </div>
      ) : (
        <MessageOffline>
          <p className='message'>Pas de connexion</p>
        </MessageOffline>
      )}
    </Container>
  )
}

export default NewMovie
