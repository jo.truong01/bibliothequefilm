import React, { useEffect } from 'react'
import LoginForm from '../components/LoginForm'
import PropTypes from 'prop-types'
import Redirect from 'react-router-dom'

const Login = ({ history }) => {
  useEffect(() => {
    const Token = localStorage.getItem('Token')

    if (Token) history.push('/home')
  }, [])

  return (
    <div>
      <LoginForm />
    </div>
  )
}
Login.propTypes = {
  history: PropTypes.object
}
export default Login
