import React, { useState } from 'react'
// components
import MyLibrary from '../components/myLibrary'
import SlideMovie from '../components/slideMovie/SlideMovie'
// styles
import Container from '../styles/Container'
import MyLibraryStyled from '../styles/MyLibraryStyled'
import Title from '../styles/Title'

const Account = props => {
  const [show, setShow] = useState(false)

  const showLibrary = () => {
    if (!show) {
      return <MyLibrary type='autre' />
    } else {
      return <MyLibrary type='library' />
    }
  }
  return (
    <>
      <MyLibraryStyled>
        <div className='title__mylibrary'>
          <Title> Ma Bibliotéque </Title>
        </div>
        <div style={{ marginTop: '50px' }}>
          <SlideMovie />
        </div>
        <div className='breadcrumb'>
          <ul>
            <li onClick={() => setShow(false)}>
              <span className={!show ? 'span__breadcrumb' : 'span__decoration'}>
                Film déja regardé
              </span>
            </li>
            <li onClick={() => setShow(true)}>
              <span className={!show ? 'span__decoration' : 'span__breadcrumb'}>
                Film à regarder
              </span>
            </li>
          </ul>
        </div>
        <div>{showLibrary()}</div>
      </MyLibraryStyled>
    </>
  )
}

export default Account
