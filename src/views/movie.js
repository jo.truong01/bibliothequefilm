import React, { useEffect, useState } from 'react'
import { Link, useParams } from 'react-router-dom'
import axios from 'axios'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import {
  addLibrary,
  addSeen,
  removeLibrary,
  removeSeen
} from '../actions/library'
import { connect } from 'react-redux'
import YouTube from 'react-youtube'
import movieTraider from 'movie-trailer'
// components
import BtnVideoPlay from '../components/BtnVideoPlay'
import Loader from '../components/loader'
import BtnLibrarySeen from '../components/btnLibrary/BtnLibrarySeen'
import BtnLibraryNotSeen from '../components/btnLibrary/btnLibraryNotSeen'
import BtnFavorite from '../components/BtnFavorite'
import Recommended from '../components/Recommended'
// styles
import Container from '../styles/Container'
import Title from '../styles/Title'
import Wrapper from '../styles/Wrapper'
import MessageOffline from '../styles/MessageOffline'
// image
import ImageError from '../assets/error.PNG'

const Movie = ({ library, seen }) => {
  const [movie, setMovie] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const [errorMessage, setErrorMessage] = useState(false)
  const [trailerUlr, setTrailerUlr] = useState('')
  let key = process.env.REACT_APP_API_KEY
  let base_urlImage = process.env.REACT_APP_BASE_URL_IMAGE
  let param = useParams()

  const opts = {
    height: '500',
    width: '100%',
    playerVars: {
      autoplay: 1
    }
  }

  const handleClick = movie => {
    if (trailerUlr) {
      setTrailerUlr('')
    } else {
      movieTraider(movie?.name || movie?.title || movie?.original_name || '')
        .then(url => {
          const urlParams = new URLSearchParams(new URL(url).search)
          setTrailerUlr(urlParams.get('v'))
        })
        .catch(error => {
          setErrorMessage(true)
        })
    }
  }

  useEffect(() => {
    getDetailMovie()
  }, [])

  const getDetailMovie = () => {
    axios
      .get(`https://api.themoviedb.org/3/movie/${param.id}?api_key=${key}`)
      .then(resp => {
        setMovie(resp.data)
        setIsLoading(false)
      })
      .catch(err => {
        setIsLoading(false)
        console.error('ERROR: ', err)
      })
  }

  if (isLoading) {
    return <Loader />
  }
  const existsInLibrary = movie => {
    return library.find(item => item.id === movie.id)
  }

  const existsInSeen = movie => {
    return seen.find(item => item.id === movie.id)
  }

  const name = () => {
    return param.name[0].toUpperCase() + param.name.slice(1)
  }

  return (
    <Container detail>
      {movie ? (
        <Wrapper>
          {!errorMessage ? (
            <div>
              {trailerUlr ? (
                <div>
                  {trailerUlr && <YouTube videoId={trailerUlr} opts={opts} />}
                </div>
              ) : (
                <ContainerImage
                  url={`url(${base_urlImage}${
                    movie?.backdrop_path || movie?.poster_path
                  })`}
                >
                  <div className='btn__play'>
                    <BtnVideoPlay movie={movie} handleClick={handleClick} />
                  </div>
                </ContainerImage>
              )}
            </div>
          ) : (
            <ContainerImage url={`url(${ImageError})`}>
              <div className='content__message'>
                <MessageOffline>
                  <p className='message'>Pas de detail pour ce film</p>
                </MessageOffline>
              </div>
            </ContainerImage>
          )}

          <div className='detail__down'>
            <div className='detail__first'>
              <div className='image__poster'>
                <img
                  src={`${base_urlImage}${
                    movie?.poster_path || movie?.backprod_path
                  }`}
                  className='img-responsive'
                  alt='Image'
                />
              </div>
              <div className='detail'>
                <div
                  className='detail__title'
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <Title>{name()}</Title>
                  <div
                    style={{
                      position: 'relative',
                      top: '-12px',
                      alignSelf: 'center',
                      marginLeft: '25px'
                    }}
                  >
                    <BtnFavorite detail top id={movie.id} />
                  </div>
                </div>

                <div className='detail__description'>{movie.overview}</div>

                <div style={{ marginTop: '20px' }}>
                  <p>
                    <label>Popularité :</label>
                    <span>{movie.popularity} </span>
                  </p>
                  <p className='detail__genres'>
                    Genre :
                    {movie.genres ? (
                      movie.genres.map(item => (
                        <ul key={item.id}>
                          <Link to={`/explore/${item.name}/${item.id}`}>
                            <li style={{ textDecoration: 'underline' }}>
                              &nbsp;{item.name},
                            </li>
                          </Link>
                        </ul>
                      ))
                    ) : (
                      <></>
                    )}
                  </p>
                </div>

                <div className='detail__buttons'>
                  <BtnLibrarySeen
                    existsInSeen={existsInSeen(movie)}
                    removeSeen={removeSeen(movie)}
                    addSeen={addSeen(movie)}
                  />
                  <BtnLibraryNotSeen
                    existsInLibrary={existsInLibrary(movie)}
                    removeLibrary={removeLibrary(movie)}
                    addLibrary={addLibrary(movie)}
                  />
                </div>
              </div>
            </div>
            <div className='detail__last'>
              <Recommended id={movie.id} type='Movie' />
            </div>
          </div>
        </Wrapper>
      ) : (
        <MessageOffline>
          <p className='message'>Pas de detail pour ce film</p>
        </MessageOffline>
      )}
    </Container>
  )
}

Movie.propTypes = {
  library: PropTypes.array,
  seen: PropTypes.array
}

const mapStateToProps = state => ({
  library: state.library.movieLibrary,
  seen: state.library.movieSeen
})

export default connect(mapStateToProps)(Movie)

const ContainerImage = styled.div`
  background-image: ${props => props.url};
  background-size: cover;
  height: 500px;
  background-position: top center;
  position: relative;
  @media screen and (max-width: 600px) {
    height: 200px !important;
  }
  .btn__play {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }

  .content__message {
    display: none;
    position: relative;
    top: 60px;
  }
  &:hover .content__message {
    display: block;
  }
`
