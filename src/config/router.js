import React from 'react'
import {
  Route,
  BrowserRouter as Router,
  Switch,
  Redirect
} from 'react-router-dom'

import PrivateRoute from '../components/PrivateRoute'

import Login from '../views/login'
import Library from '../views/libraryPage'
import Home from '../views/home'
import Movie from '../views/movie'
import NewMovie from '../views/newMovie'
import TV from '../views/tv'
import Explore from '../views/explore'
import NoLog from '../views/noLog'
import NoPage from '../views/404'

import FavoritePage from '../views/favoritePage'
import Header from '../components/Header/header'
import Footer from '../components/Footer/Footer'

import { useDarkMode } from '../styles/useDarkMode'
import { GlobalStyles, lightTheme, darkTheme } from '../styles/globalStyles'
import { ThemeProvider } from 'styled-components'
import Search from '../views/Search'
import PropType from 'prop-types'

const WRoute = props => {
  const [theme, toggleTheme] = useDarkMode()
  const themeMode = theme === 'light' ? lightTheme : darkTheme

  return props.path === '/' ? (
    <Route exact path={props.path} component={props.component} />
  ) : (
    <>
      <ThemeProvider theme={theme}>
        <GlobalStyles></GlobalStyles>
        <div className='container'>
          <Header theme={theme} toggleTheme={toggleTheme} />
          <PrivateRoute exact path={props.path} component={props.component} />
        </div>
        <Footer />
      </ThemeProvider>
    </>
  )
}

const Routes = () => {
  return (
    <Router>
      <Switch>
        <WRoute exact path='/' component={Login} />
        <WRoute path='/home' component={Home} />
        <WRoute exact path='/libraryPage' component={Library} />
        <WRoute exact path='/newMovie' component={NewMovie} />
        <WRoute exact path='/movie/:id/:name' component={Movie} />
        <WRoute exact path='/tv/:id/:name' component={TV} />
        <WRoute exact path='/explore/:name/:id' component={Explore} />
        <WRoute exact path='/favoritePage' component={FavoritePage} />
        <WRoute exact path='/search' component={Search} />
        <Route path='/noLog' component={NoLog} />
        <Route path='*' component={NoPage} />
        <Redirect to='/'></Redirect>
      </Switch>
    </Router>
  )
}

WRoute.propTypes = {
  path: PropType.any,
  component: PropType.any,
  exact: PropType.any
}

export default Routes
