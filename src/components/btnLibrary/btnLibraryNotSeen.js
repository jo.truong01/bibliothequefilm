import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
// styles
import Button from '../../styles/Button'
import DeleteBtn from '../../styles/DeleteBtn'

const BtnLibraryNotSeen = ({ existsInLibrary, removeLibrary, addLibrary }) => {
  const [exist, setExist] = useState(existsInLibrary)
  const dispatch = useDispatch()
  const disabled = () => {
    if (!exist) {
      document.querySelector('.seen').style.pointerEvents = 'none'
      document.querySelector('.btn__seen').style.opacity = 0.5
    } else {
      document.querySelector('.seen').style.pointerEvents = 'auto'
      document.querySelector('.btn__seen').style.opacity = 1
    }
  }

  const handleRemove = () => {
    setExist(false)
    dispatch(removeLibrary)
    disabled()
  }

  const handleAdd = () => {
    setExist(true)
    dispatch(addLibrary)
    disabled()
  }
  return (
    <div>
      <div className='notseen'>
        {exist ? (
          <DeleteBtn
            libray
            className='btn__notseen'
            onClick={() => dispatch(handleRemove)}
          >
            Supprimer
          </DeleteBtn>
        ) : (
          <Button
            libray
            className='btn__notseen'
            onClick={() => dispatch(handleAdd)}
          >
            Film à regarder
          </Button>
        )}
      </div>
    </div>
  )
}

BtnLibraryNotSeen.propTypes = {
  movie: PropTypes.object,
  existsInLibrary: PropTypes.object,
  removeLibrary: PropTypes.object,
  addLibrary: PropTypes.object
}
export default BtnLibraryNotSeen
