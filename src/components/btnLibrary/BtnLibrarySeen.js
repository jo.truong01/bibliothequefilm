import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useDispatch } from 'react-redux'
// styles
import Button from '../../styles/Button'
import DeleteBtn from '../../styles/DeleteBtn'
const BtnLibrary = ({ existsInSeen, removeSeen, addSeen }) => {
  const [exist, setExist] = useState(existsInSeen)
  const dispatch = useDispatch()

  const disabled = () => {
    if (!exist) {
      document.querySelector('.notseen').style.pointerEvents = 'none'
      document.querySelector('.btn__notseen').style.opacity = 0.5
    } else {
      document.querySelector('.notseen').style.pointerEvents = 'auto'
      document.querySelector('.btn__notseen').style.opacity = 1
    }
  }
  const handleRemove = () => {
    setExist(false)
    dispatch(removeSeen)
    disabled()
  }

  const handleAdd = () => {
    setExist(true)
    dispatch(addSeen)
    disabled()
  }
  return (
    <div>
      <div className='seen' disabled>
        {exist ? (
          <DeleteBtn libray className='btn__seen' onClick={handleRemove}>
            Supprimer
          </DeleteBtn>
        ) : (
          <Button libray className='btn__seen' onClick={handleAdd}>
            Film Vu
          </Button>
        )}
      </div>
    </div>
  )
}

BtnLibrary.propTypes = {
  existsInSeen: PropTypes.object,
  movie: PropTypes.object,
  removeSeen: PropTypes.object,
  addSeen: PropTypes.object
}

export default BtnLibrary
