import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { removeLibrary, removeSeen } from '../../actions/library'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
// styles
import Title from '../../styles/Title'
import Container from '../../styles/Container'
import Card from '../../styles/Cards'
// images
import Play from '../../assets/play.png'
import IconCheck from '../../assets/svg/checkmark-outline.svg'

const MyLibrary = props => {
  const dispatch = useDispatch()
  let base_urlImage = process.env.REACT_APP_BASE_URL_IMAGE

  return (
    <Container Explore>
      <Title>{props.type === 'library' ? '' : ''}</Title>
      <Card>
        {props.type === 'library' // se sont des connards
          ? props.library.map(movie => (
              <div key={movie.id} className='explore__content explore__poster'>
                <Link
                  to={
                    movie.name
                      ? `/tv/${movie.id}/${movie.name}`
                      : `/movie/${movie.id}/${movie.title}`
                  }
                >
                  <div className='contents'>
                    <div className='icon iconb'>
                      <img src={Play} alt='Image' />
                    </div>
                    <img
                      src={`${base_urlImage}${
                        movie?.backdrop_path || movie?.poster_path
                      }`}
                      className='img-responsive'
                      alt={movie?.title || movie?.name}
                    />
                  </div>
                </Link>
                <BtnContent>
                  <div
                    className='btn__remove'
                    onClick={() => dispatch(removeLibrary(movie))}
                  >
                    <img src={IconCheck} alt='Image' />
                  </div>
                  <span> Supprimé </span>
                </BtnContent>
              </div>
            ))
          : props.seen.map(movie => (
              <div key={movie.id} className='explore__content explore__poster'>
                <Link
                  to={
                    movie.name
                      ? `/tv/${movie.id}/${movie.name}`
                      : `/movie/${movie.id}/${movie.title}`
                  }
                >
                  <div className='contents'>
                    <div className='icon iconb'>
                      <img src={Play} alt='Image' />
                    </div>
                    <img
                      src={`${base_urlImage}${
                        movie?.backdrop_path || movie?.poster_path
                      }`}
                      className='img-responsive'
                      alt={movie?.title || movie?.name}
                    />
                  </div>
                </Link>
                <BtnContent>
                  <div
                    className='btn__remove'
                    onClick={() => dispatch(removeSeen(movie))}
                  >
                    <img src={IconCheck} alt='Image' />
                  </div>
                  <span> Supprimé </span>
                </BtnContent>
              </div>
            ))}
      </Card>
    </Container>
  )
}

MyLibrary.propTypes = {
  type: PropTypes.string,
  library: PropTypes.array,
  seen: PropTypes.array
}
const mapStateToProps = state => ({
  library: state.library.movieLibrary,
  seen: state.library.movieSeen
})

export default connect(mapStateToProps)(MyLibrary)

const BtnContent = styled.div`
  display: flex;
  width: 95%;
  margin: 10px auto;
  .btn__remove {
    border-radius: 50%;
    position: relative;
    width: 30px;
    padding: 7px 9px;
    margin-right: 10px;
    cursor: pointer;
    transition: all 0.2s;
    &:hover {
      opacity: 0.8;
    }

    background-color: #bf2b33b6;
  }
  span {
    align-self: center;
    font-size: 16px;
    font-weight: 700;
  }
`
