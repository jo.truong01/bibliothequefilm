import React, { useState, useEffect } from 'react'
import axios from '../../services/axios'
import service from '../../services'
import BtnVideoPlay from '../BtnVideoPlay'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import YouTube from 'react-youtube'
import movieTraider from 'movie-trailer'
// image
import ImageError from '../../assets/error.PNG'

const Banner = () => {
  const [movie, setMovie] = useState([])
  const [trailerUlr, setTrailerUlr] = useState('')
  const [errorMessage, setErrorMessage] = useState(false)
  let base_urlImage = process.env.REACT_APP_BASE_URL_IMAGE
  const regex = / /gi

  const opts = {
    height: '500',
    width: '100%',
    playerVars: {
      autoplay: 1
    }
  }
  useEffect(() => {
    async function fetchData() {
      const request = await axios.get(service.fetchTvOriginals)
      setMovie(
        request.data.results[
          Math.floor(Math.random() * request.data.results.length - 1)
        ]
      )

      return request
    }
    fetchData()
  }, [])

  const handleClick = movie => {
    if (trailerUlr) {
      setTrailerUlr('')
    } else {
      movieTraider(movie?.name || movie?.title || movie?.original_name || '')
        .then(url => {
          const urlParams = new URLSearchParams(new URL(url).search)
          setTrailerUlr(urlParams.get('v'))
        })
        .catch(error => {
          setErrorMessage(true)
        })
    }
  }

  function truncate(str, n) {
    return str?.length > n ? str.substr(0, n - 1) + '...' : str
  }

  return movie ? (
    <div>
      {!errorMessage ? (
        <div>
          {trailerUlr ? (
            <div>
              {trailerUlr && <YouTube videoId={trailerUlr} opts={opts} />}
            </div>
          ) : (
            <Banners
              url={`url(${base_urlImage}${
                movie?.backdrop_path || movie?.poster_path
              })`}
            >
              <div className='banner__contents'>
                <h1 className='banner__title'>
                  {movie?.title || movie?.name || movie?.original_name}
                </h1>
                {trailerUlr && <YouTube videoId={trailerUlr} opts={opts} />}
                <div className='banner__buttons'>
                  <BtnVideoPlay movie={movie} handleClick={handleClick} />
                  <Link
                    to={
                      movie.name
                        ? `/tv/${movie?.id}/${movie?.name?.toLowerCase()}`
                        : `/movie/${movie?.id}/${movie?.title
                            ?.replace(regex, '-')
                            .toLowerCase()}`
                    }
                  >
                    Plus d&acute;infos
                  </Link>
                </div>
                <p className='banner__description'>
                  {truncate(movie?.overview, 200)}
                </p>
              </div>
            </Banners>
          )}
        </div>
      ) : (
        <div>
          <Banners url={`url(${ImageError})`}></Banners>
        </div>
      )}
    </div>
  ) : (
    <></>
  )
}

export default Banner

const Banners = styled.div`
  background-image: ${props => props.url};
  background-size: cover;
  background-position: center center;
  height: 80vh;
  color: #fff;
  margin-top: 2px;
  .banner__contents {
    margin-left: 4.5rem;
    padding-top: 10rem;
    height: 190px;
    @media screen and (max-width: 800px) {
      margin-left: 2.5rem;
    }
    @media screen and (max-width: 450px) {
      margin-left: 1.5rem;
    }
  }
  .banner__title {
    margin: 0;
    font-size: 2.7rem;
    font-weight: 800;
    @media screen and (max-width: 800px) {
      font-size: 2rem;
    }
  }
  .banner__description {
    width: 70%;
    line-height: 1.3;
    padding-top: 1rem;
    font-size: 21px;
    margin: 0;
    max-width: 560px;
    height: 80px;
    @media screen and (max-width: 800px) {
      font-size: 18px;
    }
    @media screen and (max-width: 450px) {
      font-size: 16px;
      width: 90%;
    }
  }
  .banner__buttons {
    display: flex;
    margin: 30px 0 0 0;
    @media screen and (max-width: 800px) {
      margin: 20px 0 0 0;
    }
  }
  a {
    align-self: center;
    border: none;
    cursor: pointer;
    outline: none;
    font-weight: 700;
    border-radius: 5px;
    padding: 0.7rem 2rem;
    font-size: 20px;
    color: #141414;
    margin-left: 1rem;
    background-color: #e4ecf5b4;
    transition: all 0.3s ease-in-out;
    text-decoration: none;
    @media screen and (max-width: 800px) {
      font-size: 18px;
      padding: 0.5rem 1rem;
    }
    @media screen and (max-width: 450px) {
      font-size: 17px;
      padding: 0.5rem 1rem;
    }
    &:hover {
      opacity: 0.8;
    }
  }
`
