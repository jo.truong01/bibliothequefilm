import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { addFavorite, removeFavorite } from '../../actions/favorite'
import PropTypes from 'prop-types'
import styled from 'styled-components'
// image
import Plus from '../../assets/plus.png'
import Check from '../../assets/check.png'

const BtnFavorite = ({ movie }) => {
  const dispatch = useDispatch()
  const fav = useSelector(state => state.favorite.movieFavorite)

  return (
    <LikeDislikeContainer>
      {fav.indexOf(movie) == -1 ? (
        <div className='btn__favorie'>
          <Button onClick={() => dispatch(addFavorite(movie))}>
            <img src={Plus} className='img-responsive' alt='Image' />
          </Button>
        </div>
      ) : (
        <div className='btn__favorie'>
          <Button onClick={() => dispatch(removeFavorite(movie))}>
            <img src={Check} className='img-responsive' alt='Image' />
          </Button>
        </div>
      )}
    </LikeDislikeContainer>
  )
}

BtnFavorite.propTypes = {
  movie: PropTypes.object
}

const LikeDislikeContainer = styled.div`
  margin-bottom: 1px;
  .btn__favorie {
    position: relative;
    text-align: center;
  }
`

const Button = styled.div`
  text-align: center;
  border-radius: 50%;
  position: relative;
  cursor: pointer;
  transition: all 0.2s;
  width: 40px;
  margin: 0 auto;
  img {
    width: 100%;
  }
  &:hover {
    opacity: 0.8;
  }
`

export default BtnFavorite
