import React from 'react'
import styled from 'styled-components'
import image from '../../assets/netflixfilmserieastuce.jpg'

const BackgroundImage = () => {
  return (
    <Container>
      <div
        style={{
          backgroundImage: `url(${image})`,
          height: '100vh',
          backgroundPosition: 'center center',
          backgroundSize: 'cover'
        }}
      >
        {/* <img src={image} className='img-responsive' alt='Image' /> */}
      </div>
    </Container>
  )
}

export default BackgroundImage

const Container = styled.div`
  height: 100vh;
  background-color: #000;
`
const Div = styled.div`
  img {
    width: 100%;
    /* position: fixed; */
    height: 100vh;
    object-fit: cover;
  }
`
