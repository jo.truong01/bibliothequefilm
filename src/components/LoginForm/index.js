import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import Axios from 'axios'
import styled from 'styled-components'
// component
import BackgroundImage from './backgroundImage'
// image
import logo from '../../assets/logo.png'

const LoginForm = () => {
  const [email, setEmail] = useState('')
  const [mdp, setMdp] = useState('')
  const [errorMessage, setErrorMessage] = useState('')
  let history = useHistory()

  const handleEmail = event => {
    setEmail(event.target.value)
  }

  const handleMdp = event => {
    setMdp(event.target.value)
  }

  const handleLog = e => {
    e.preventDefault()
    const cred = { username: email, password: mdp }
    try {
      if (!email || !mdp) {
        setErrorMessage('Veuillez remplir les champs')
        if (!email) {
          setErrorMessage('Veuillez entrer un mail')
          return
        } else if (!mdp) {
          setErrorMessage(' Veuillez entrer un mot de passe')
          return
        }
        return
      } else if (mdp.length < 6) {
        setErrorMessage(
          'Veuillez entrer un mot de passe superieur a 5 caractéres'
        )
        return
      }
      Axios.post('https://easy-login-api.herokuapp.com/users/login', cred).then(
        resp => {
          localStorage.setItem('Token', resp.headers['x-access-token'])
          history.push('/home')
        }
      )
    } catch (error) {
      console.log('ERROR: ', error)
    }
  }

  return (
    <ContainerLogin>
      <BackgroundImage />
      <Container>
        <div className='logo'>
          <img src={logo} className='img-responsive' alt='Image' />
        </div>
        <Card>
          <FormTitle>
            <span> Se Connecter </span>
          </FormTitle>
          <Form>
            <FormGroup>
              <input
                type='email'
                name='email'
                placeholder='Email'
                className='effect'
                onChange={handleEmail}
              />
              <div className='underline'></div>
              <p className='title'>
                Veuillez entrer une adresse e-mail valide.
              </p>
            </FormGroup>
            <FormGroup last>
              <input
                type='password'
                placeholder='Mot de passe'
                name='mdp'
                className='effect'
                onChange={handleMdp}
              />
              <div className='underline'></div>
              <p className='title'>
                Votre mot de passe doit comporter entre 4 et 60 caractères.
              </p>
            </FormGroup>
            <div
              style={{ margin: '15px 0', color: 'red', textAlign: 'center' }}
            >
              <span className='error'>{errorMessage}</span>
            </div>
            <span className='text'>
              Cette page est protégée par un système d&apos; authentification
            </span>
            <Button onClick={handleLog}> Connexion </Button>
          </Form>
        </Card>
      </Container>
    </ContainerLogin>
  )
}

export default LoginForm

const ContainerLogin = styled.div`
  position: relative;
  font-family: 'Open Sans', sans-serif;
`
const Container = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  .logo {
    margin-top: 25px;
    margin-left: 25px;
    img {
      width: 150px;
      @media screen and (max-width: 450px) {
        width: 100px;
      }
    }
  }
  /* background-color: rgba(20, 20, 20, 0.97); */
`

const Card = styled.div`
  position: fixed;
  top: 48%;
  left: 50%;
  transform: translate(-50%, -50%);
  border: 1px solid rgba(20, 20, 20, 0.67);
  border-radius: 5px;
  background-color: rgba(20, 20, 20, 0.67);
  padding: 45px;
  width: 310px;
  @media screen and (max-width: 450px) {
    width: 270px;
    padding: 20px;
  }
`
const Form = styled.div`
  width: 100%;
  .text {
    color: #fff;
    letter-spacing: 1px;
    line-height: 29px;
    font-size: 14px;
    @media screen and (max-width: 450px) {
      font-size: 13px;
      line-height: 22px;
    }
  }
`
const FormGroup = styled.div`
  position: relative;
  width: 100%;
  margin: 20px 0;
  input {
    padding: 14px 12px;
    margin-bottom: 5px;
    width: 96.2%;
    outline: none;
    font-size: 16px;
    transform: all 0.4s ease;
    position: relative;
    border: transparent;
    border-bottom: 2px solid transparent;
    &:focus {
      transform: all 0.4s ease;
      border-bottom: 2px solid #4071ad;
    }
    @media screen and (max-width: 450px) {
      padding: 10px 5px;
      font-size: 15px;
    }
  }
  .title {
    margin: 0;
    text-align: center;
    color: #4071ad;
    font-size: 14px;
    @media screen and (max-width: 450px) {
      font-size: 13px;
    }
  }
`

const FormTitle = styled.div`
  margin-bottom: 25px;
  span {
    color: #ffffff;
    font-size: 30px;
    font-weight: 700;
    font-family: 'Open Sans', sans-serif;
    @media screen and (max-width: 450px) {
      font-size: 23px;
    }
  }
`
const Button = styled.button`
  margin-top: 15px;
  line-height: 50px;
  border: none;
  outline: none;
  border-radius: 5px;
  background-color: #4071ad;
  color: #fff;
  font-size: 18px;
  width: 100%;
  cursor: pointer;
  transition: all 0.4s ease-in-out;
  &:hover {
    transition: all 0.4s ease-in-out;
    opacity: 0.7;
  }
  @media screen and (max-width: 450px) {
    font-size: 16px;
    line-height: 40px;
  }
`
