import React from 'react'
import PropTypes from 'prop-types'
import Card from '../../styles/Cards'
import { Link } from 'react-router-dom'
// component
import BtnFavorite from '../BtnFavorite'
// style
import Play from '../../assets/play.png'
// image
import MessageOffline from '../../styles/MessageOffline'

const PopularMovie = ({ movieForm, handleMovie }) => {
  let base_urlImage = process.env.REACT_APP_BASE_URL_IMAGE
  return (
    <div>
      {movieForm ? (
        <Card>
          {movieForm.map(movieForm => (
            <div
              key={movieForm.id}
              className={
                movieForm?.backdrop_path ? 'explore__content' : 'ishide'
              }
            >
              <Link
                to={
                  movieForm.name
                    ? `/tv/${movieForm.id}/${movieForm.name}`
                    : `/movie/${movieForm.id}/${movieForm.title}`
                }
              >
                <div className='contents'>
                  <div className='icon iconb'>
                    <img src={Play} alt='Image' />
                  </div>
                  <img
                    src={`${base_urlImage}${movieForm?.backdrop_path}`}
                    className='img-responsive'
                    alt={movieForm?.title || movieForm?.name}
                  />
                </div>
              </Link>
              <div>
                <BtnFavorite movie={movieForm}></BtnFavorite>
              </div>
            </div>
          ))}
        </Card>
      ) : (
        <MessageOffline>
          <p className='message'>Pas de connexion</p>
        </MessageOffline>
      )}
    </div>
  )
}

PopularMovie.propTypes = {
  movieForm: PropTypes.array,
  handleMovie: PropTypes.func
}
export default PopularMovie
