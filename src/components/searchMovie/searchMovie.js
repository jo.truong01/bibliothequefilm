import React from 'react'
import PropTypes from 'prop-types'
import Card from '../../styles/Cards'
import { Link } from 'react-router-dom'
// component
import BtnFavorite from '../BtnFavorite'
// style
import Play from '../../assets/play.png'
const SearchMovie = ({ searchTerm }) => {
  let base_urlImage = process.env.REACT_APP_BASE_URL_IMAGE

  return (
    <>
      {searchTerm.length > 0 ? (
        <Card>
          {searchTerm.map(searchTerms => (
            <div
              key={searchTerms.id}
              className={
                searchTerms?.backdrop_path ? 'explore__content' : 'ishide'
              }
            >
              <Link
                to={
                  searchTerms.name
                    ? `/tv/${searchTerms.id}/${searchTerms.name}`
                    : `/movie/${searchTerms.id}/${searchTerms.title}`
                }
              >
                <div className='contents'>
                  <div className='icon'>
                    <img src={Play} alt='Image' />
                  </div>
                  <img
                    src={`${base_urlImage}${searchTerms?.backdrop_path}`}
                    className='img-responsive'
                    alt={searchTerms?.title || searchTerms?.name}
                  />
                </div>
              </Link>
              <div>
                <BtnFavorite movie={searchTerms}></BtnFavorite>
              </div>
            </div>
          ))}
        </Card>
      ) : (
        <></>
      )}
    </>
  )
}

SearchMovie.propTypes = {
  searchTerm: PropTypes.any
}
export default SearchMovie
