import React from 'react'
import LoaderMovie from '../../styles/Loader'
const Loader = () => {
  return (
    <LoaderMovie>
      <div className='movie-card-loader'>
        <div className='shimmer-block'>
          <div className='right-column'>
            <div className='shimmer-mask rating-box'></div>
            <div className='shimmer-mask votes-box'></div>
            <div className='shimmer-mask book-button-box'></div>
          </div>
          <div className='left-column'></div>
          <div className='content-column'>
            <div className='shimmer-mask poster-right'></div>
            <div className='shimmer-mask title-bottom'></div>
            <div className='shimmer-mask language-bottom'></div>
            <div className='shimmer-mask genre-bottom'></div>
            <div className='shimmer-mask title-right'></div>
            <div className='shimmer-mask language-right'></div>
            <div className='shimmer-mask genre-right'></div>
            <div className='shimmer-mask tags-right'></div>
          </div>
        </div>
      </div>
      <div className='movie-card-loader'>
        <div className='shimmer-block'>
          <div className='right-column'>
            <div className='shimmer-mask rating-box'></div>
            <div className='shimmer-mask votes-box'></div>
            <div className='shimmer-mask book-button-box'></div>
          </div>
          <div className='left-column'></div>
          <div className='content-column'>
            <div className='shimmer-mask poster-right'></div>
            <div className='shimmer-mask title-bottom'></div>
            <div className='shimmer-mask language-bottom'></div>
            <div className='shimmer-mask genre-bottom'></div>
            <div className='shimmer-mask title-right'></div>
            <div className='shimmer-mask language-right'></div>
            <div className='shimmer-mask genre-right'></div>
            <div className='shimmer-mask tags-right'></div>
          </div>
        </div>
      </div>
      <div className='movie-card-loader'>
        <div className='shimmer-block'>
          <div className='right-column'>
            <div className='shimmer-mask rating-box'></div>
            <div className='shimmer-mask votes-box'></div>
            <div className='shimmer-mask book-button-box'></div>
          </div>
          <div className='left-column'></div>
          <div className='content-column'>
            <div className='shimmer-mask poster-right'></div>
            <div className='shimmer-mask title-bottom'></div>
            <div className='shimmer-mask language-bottom'></div>
            <div className='shimmer-mask genre-bottom'></div>
            <div className='shimmer-mask title-right'></div>
            <div className='shimmer-mask language-right'></div>
            <div className='shimmer-mask genre-right'></div>
            <div className='shimmer-mask tags-right'></div>
          </div>
        </div>
      </div>
      <div className='movie-card-loader'>
        <div className='shimmer-block'>
          <div className='right-column'>
            <div className='shimmer-mask rating-box'></div>
            <div className='shimmer-mask votes-box'></div>
            <div className='shimmer-mask book-button-box'></div>
          </div>
          <div className='left-column'></div>
          <div className='content-column'>
            <div className='shimmer-mask poster-right'></div>
            <div className='shimmer-mask title-bottom'></div>
            <div className='shimmer-mask language-bottom'></div>
            <div className='shimmer-mask genre-bottom'></div>
            <div className='shimmer-mask title-right'></div>
            <div className='shimmer-mask language-right'></div>
            <div className='shimmer-mask genre-right'></div>
            <div className='shimmer-mask tags-right'></div>
          </div>
        </div>
      </div>
      <div className='movie-card-loader'>
        <div className='shimmer-block'>
          <div className='right-column'>
            <div className='shimmer-mask rating-box'></div>
            <div className='shimmer-mask votes-box'></div>
            <div className='shimmer-mask book-button-box'></div>
          </div>
          <div className='left-column'></div>
          <div className='content-column'>
            <div className='shimmer-mask poster-right'></div>
            <div className='shimmer-mask title-bottom'></div>
            <div className='shimmer-mask language-bottom'></div>
            <div className='shimmer-mask genre-bottom'></div>
            <div className='shimmer-mask title-right'></div>
            <div className='shimmer-mask language-right'></div>
            <div className='shimmer-mask genre-right'></div>
            <div className='shimmer-mask tags-right'></div>
          </div>
        </div>
      </div>
    </LoaderMovie>
  )
}

export default Loader
