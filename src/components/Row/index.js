import React, { useState, useEffect } from 'react'
import axios from '../../services/axios'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import Swiper, { Navigation, Pagination } from 'swiper'
import 'swiper/swiper-bundle.css'
// component
import Loader from '../loader'
import BtnFavorite from '../BtnFavorite'
// styles
import Title from '../../styles/Title'
import Rows from '../../styles/Rows'
// images
import Play from '../../assets/play.png'

Swiper.use([Navigation, Pagination])

const Row = ({ withGenres, title, fetchUrl, isPosterRow, isExplore }) => {
  const [movies, setMovies] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const base_UrlImage = 'https://image.tmdb.org/t/p/original/'
  const regex = / /gi

  const initSwiper = () => {
    let mySwiper = new Swiper('.swiper-container', {
      // Optional parameters
      slidesPerView: 6,
      spaceBetween: 30,
      slidesPerGroup: 1,
      loop: true,

      // Navigation arrows
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      breakpoints: {
        320: {
          slidesPerView: 2,
          spaceBetween: 20
        },
        550: {
          slidesPerView: 2,
          spaceBetween: 30
        },
        750: {
          slidesPerView: 3,
          spaceBetween: 40
        },
        850: {
          slidesPerView: 4,
          spaceBetween: 40
        },
        1050: {
          slidesPerView: 5,
          spaceBetween: 30
        },
        1260: {
          slidesPerView: 6,
          spaceBetween: 30
        }
      }
    })
  }
  //  Appel de l'api sur l'object fetchUrl
  useEffect(() => {
    async function fetchData() {
      const request = await axios.get(fetchUrl)
      setMovies(request.data.results)
      setIsLoading(false)
      initSwiper()
      return request
    }
    fetchData()
  }, [fetchUrl])

  if (isLoading) {
    return <Loader />
  }

  return (
    <Rows>
      {movies.length > 0 ? (
        <div>
          <div className='row__title'>
            <Title className='title'> {title} </Title>
            <span>
              {!isExplore ? (
                <Link
                  to={`/explore/${title
                    .replace(regex, '-')
                    .toLowerCase()}/${withGenres}`}
                >
                  Voir plus
                </Link>
              ) : (
                <></>
              )}
            </span>
          </div>

          <div
            className={
              isPosterRow
                ? 'swiper-container poster'
                : 'swiper-container backdrop'
            }
          >
            <div className='swiper-wrapper'>
              {movies.length > 0 ? (
                movies.map(movie => (
                  <div
                    key={movie.id}
                    className={
                      (isPosterRow
                        ? 'swiper-slide card__poster'
                        : 'swiper-slide card__backdrop',
                      movie?.backdrop_path
                        ? 'swiper-slide card__poster'
                        : 'ishide')
                    }
                  >
                    <Link
                      to={
                        movie.name
                          ? `/tv/${movie.id}/${movie.name
                              .replace(' ', '-')
                              .toLowerCase()}`
                          : `/movie/${movie.id}/${movie.title
                              .replace(regex, '-')
                              .toLowerCase()}`
                      }
                    >
                      <div className={isPosterRow ? 'cardp' : 'cardb'}>
                        <div className='icon'>
                          <img src={Play} alt='Image' />
                        </div>
                        <img
                          className={
                            isPosterRow ? 'row__poster' : 'row__backdrop'
                          }
                          src={`${base_UrlImage}${
                            isPosterRow
                              ? movie?.poster_path
                              : movie?.backdrop_path
                          }`}
                          alt={movie?.title || movie?.name}
                        />
                      </div>
                    </Link>
                    <BtnFavorite movie={movie}></BtnFavorite>
                  </div>
                ))
              ) : (
                <p> Pas de films Actuellement </p>
              )}
            </div>
            <div
              className={
                isPosterRow
                  ? 'swiper-button-prev'
                  : 'swiper-button-prev btn__prev'
              }
            ></div>
            <div
              className={
                isPosterRow
                  ? 'swiper-button-next'
                  : 'swiper-button-next btn__next'
              }
            ></div>
          </div>
        </div>
      ) : (
        <p style={{ textAlign: 'center', fontSize: '25px', fontWeight: '700' }}>
          Pas de films actuelemnt
        </p>
      )}
    </Rows>
  )
}

Row.propTypes = {
  title: PropTypes.string,
  fetchUrl: PropTypes.string,
  withGenres: PropTypes.number,
  isPosterRow: PropTypes.bool,
  isExplore: PropTypes.bool
}

export default Row
