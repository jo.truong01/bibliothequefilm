import Axios from 'axios'
import PropTypes from 'prop-types'
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
// styles
import Title from '../../styles/Title'
import Card from '../../styles/Cards'
import MessageOffline from '../../styles/MessageOffline'
// image
import Play from '../../assets/play.png'

const Recommended = ({ id, type }) => {
  const [recommendList, setRecommend] = useState([])

  let key = process.env.REACT_APP_API_KEY
  let url = ''
  let url_image = process.env.REACT_APP_BASE_URL_IMAGE

  useEffect(() => {
    if (type === 'TV')
      url = `https://api.themoviedb.org/3/tv/${id}/recommendations?api_key=${key}`
    if (type === 'Movie')
      url = `https://api.themoviedb.org/3/movie/${id}/recommendations?api_key=${key}`

    Axios.get(url)
      .then(resp => {
        setRecommend(resp.data.results)
      })
      .catch(err => console.error('ERROR : ', err))
  }, [])

  return (
    <div>
      <Title style={{ textAlign: 'center' }}> Recommendation </Title>
      <>
        {recommendList.length > 0 ? (
          <Card>
            {recommendList.map(movie => (
              <div
                key={movie.id}
                className={movie?.backdrop_path ? 'explore__content' : 'ishide'}
              >
                <Link
                  to={
                    movie.name
                      ? `/tv/${movie?.id}/${movie?.name
                          .replace(' ', '-')
                          .toLowerCase()}`
                      : `/movie/${movie?.id}/${movie?.title
                          .replace(/ /gi, '-')
                          .toLowerCase()}`
                  }
                >
                  <div className='contents'>
                    <div className='icon iconb'>
                      <img src={Play} alt='Image' />
                    </div>
                    <img src={`${url_image}${movie.backdrop_path}`} />
                  </div>
                </Link>
              </div>
            ))}
          </Card>
        ) : (
          <MessageOffline>
            <p className='message'>Pas de films</p>
          </MessageOffline>
        )}
      </>
    </div>
  )
}

Recommended.propTypes = {
  id: PropTypes.number,
  type: PropTypes.string
}

export default Recommended
