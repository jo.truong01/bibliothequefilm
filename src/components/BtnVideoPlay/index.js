import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
// image
import Play from '../../assets/play.png'
// styles
import BtnVideo from '../../styles/BtnVideo'

const VideoMovie = ({ handleClick, movie }) => {
  return (
    <div>
      <BtnVideo detail onClick={() => handleClick(movie)}>
        <img src={Play} alt='Image' />
      </BtnVideo>
    </div>
  )
}

VideoMovie.propTypes = {
  movie: PropTypes.any,
  handleClick: PropTypes.func
}

export default VideoMovie
