import React, { useState, useEffect } from 'react'
import Swiper, { Navigation, Pagination } from 'swiper'
import Loader from '../loader'
import axios from '../../services/axios'
import services from '../../services'
// style
import Slide from '../../styles/Slide'

const SlideMovie = () => {
  const [movies, setMovies] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  let base_urlImage = process.env.REACT_APP_BASE_URL_IMAGE

  const initSwiper = () => {
    let swiperLibrary = new Swiper('.swiper__library', {
      spaceBetween: 20,
      centeredSlides: true,
      loop: true,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      }
    })
  }

  useEffect(() => {
    async function fetchData() {
      const request = await axios.get(services.fetchPopular)
      setMovies(request.data.results)
      setIsLoading(false)
      initSwiper()
      return request
    }
    fetchData()
  }, [])

  if (isLoading) {
    return <Loader />
  }

  return (
    <Slide>
      <div className='swiper-container swiper__library'>
        <div className='swiper-wrapper'>
          {movies.map(movie => (
            <div className='swiper-slide' key={movie.id}>
              {/* <p> {movie?.title} </p> */}
              <div className='swiper-slide  swiper__image'>
                <div
                  className='back__image'
                  style={{
                    backgroundImage: `url(${base_urlImage}${movie?.backdrop_path})`
                  }}
                ></div>
              </div>
            </div>
          ))}
        </div>
        <div className='swiper-button-next'></div>
        <div className='swiper-button-prev'></div>
      </div>
    </Slide>
  )
}

export default SlideMovie
