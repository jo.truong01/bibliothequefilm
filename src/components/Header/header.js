/* eslint-disable react/jsx-no-undef */
import React, { useState } from 'react'
import styled from 'styled-components'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
// component
import NavBar from './NavBar'
// images
import Fr from '../../assets/fr.webp'
import En from '../../assets/en.jpg'
import Powers from '../../assets/logout2.png'

// eslint-disable-next-line react/prop-types
const Toggle = ({ theme = 'light', toggleTheme }) => {
  let Sun, Moon
  const [toggle, setToggle] = useState(false)
  const history = useHistory()

  Sun = Moon = styled.svg`
    transition: all 0.5s linear;
  `

  const deconnexion = () => {
    localStorage.removeItem('Token')
    history.push('/')
  }

  const { t, i18n } = useTranslation()

  return (
    <div
      className='header'
      style={{ height: '90px', boxShadow: '0px 2px 11px 0px rgba(0,0,0,0.25)' }}
    >
      <Container>
        <div>
          <NavBar />
        </div>

        <div className='button'>
          <StyleContainer onClick={toggleTheme}>
            {theme.name === 'light' ? (
              <Moon
                xmlns='http://www.w3.org/2000/svg'
                width='41'
                height='41'
                viewBox='0 0 24 24'
                fill='none'
                stroke='#212121'
                strokeWidth='2'
                strokeLinecap='round'
                strokeLinejoin='round'
                className='feather feather-moon'
              >
                <path d='M21 12.79A9 9 0 1 1 11.21 3 7 7 0 0 0 21 12.79z'></path>
              </Moon>
            ) : (
              <Sun
                xmlns='http://www.w3.org/2000/svg'
                width='41'
                height='41'
                viewBox='0 0 24 24'
                fill='none'
                stroke='#fff'
                strokeWidth='2'
                strokeLinecap='round'
                strokeLinejoin='round'
                className='feather feather-sun'
              >
                <circle cx='12' cy='12' r='5'></circle>
                <line x1='12' y1='1' x2='12' y2='3'></line>
                <line x1='12' y1='21' x2='12' y2='23'></line>
                <line x1='4.22' y1='4.22' x2='5.64' y2='5.64'></line>
                <line x1='18.36' y1='18.36' x2='19.78' y2='19.78'></line>
                <line x1='1' y1='12' x2='3' y2='12'></line>
                <line x1='21' y1='12' x2='23' y2='12'></line>
                <line x1='4.22' y1='19.78' x2='5.64' y2='18.36'></line>
                <line x1='18.36' y1='5.64' x2='19.78' y2='4.22'></line>
              </Sun>
            )}
          </StyleContainer>
          <BtnLanguage
            className='language'
            style={{ alignSelf: 'center', marginLeft: '15px' }}
          >
            <div className='fr'>
              <img
                onClick={() => i18n.changeLanguage('fr')}
                src={Fr}
                className='img-responsive'
                alt='Image'
              />
            </div>
            <div className='en'>
              <img
                onClick={() => i18n.changeLanguage('en')}
                src={En}
                className='img-responsive'
                alt='Image'
              />
            </div>
          </BtnLanguage>
          <div className='user'>
            <img
              onClick={() => deconnexion()}
              src={Powers}
              className='img-responsive'
              alt='Image'
            />
          </div>
        </div>
      </Container>
    </div>
  )
}

export default Toggle

const StyleContainer = styled.div`
  align-self: center;
  height: 50px;
  width: 50px;
  background-color: ${props => props.theme.body};
  @media screen and (max-width: 850px) {
    width: 35px;
    height: 45px;
  }
  @media screen and (max-width: 530px) {
    width: 25px;
    height: 37px;
  }
  .feather {
    @media screen and (max-width: 850px) {
      width: 35px;
      height: 35px;
    }
    @media screen and (max-width: 530px) {
      width: 30px;
      height: 30px;
    }
  }
`

const Container = styled.div`
  display: flex;
  justify-content: space-between;

  .button {
    padding-top: 20px;
    margin-right: 50px;
    display: flex;
    @media screen and (max-width: 850px) {
      padding-top: 22px;
    }
    @media screen and (max-width: 530px) {
      margin-right: 5px;
      padding-top: 27px;
    }
  }
  .user {
    align-self: center;
    img {
      cursor: pointer;
      width: 40px;
      @media screen and (max-width: 530px) {
        width: 35px;
      }
    }
  }
`
const BtnLanguage = styled.div`
  cursor: pointer;
  .fr {
    float: left;
    margin-right: 10px;
  }
  .en {
    float: right;
    margin-right: 10px;
  }
  img {
    width: 35px;
    height: 25px;
  }
  @media screen and (max-width: 850px) {
    img {
      width: 30px;
      height: 20px;
    }
  }
  @media screen and (max-width: 450px) {
    .fr {
      margin-right: 5px;
    }
    .en {
      margin-right: 5px;
    }
    img {
      width: 20px;
      height: 15px;
    }
  }
`
