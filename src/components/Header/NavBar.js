import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import Logo from '../../assets/logo.png'
import { useTranslation } from 'react-i18next'
// styles
import NavBars from '../../styles/NavBars'
const NavBar = () => {
  const { t, i18n } = useTranslation()
  const [showNav, setShowNav] = useState(false)

  const handleToggle = () => {
    if (showNav) {
      setShowNav(false)
      document.querySelector('.item__ul').classList.remove('show__menu')
    } else {
      setShowNav(true)
      document.querySelector('.item__ul').classList.add('show__menu')
    }
  }

  return (
    <NavBars>
      <div className='logo'>
        <Link to='/home'>
          <img src={Logo} className='img-responsive' alt='Image' />
        </Link>
      </div>
      <div className='link__menu'>
        <ul className='item__menu'>
          <li>
            <button onClick={() => handleToggle()} className='parcourir'>
              Parcourir
            </button>
            <ul className='item__ul'>
              <li>
                <Link to='/home'> {t('header.home')}</Link>
              </li>
              <li>
                <Link to='/newMovie'> {t('header.nouveauté')}</Link>
              </li>
              <li>
                <Link to='/favoritePage'> {t('header.maliste')}</Link>
              </li>
              <li>
                <Link to='/libraryPage'> {t('header.mabibioteque')}</Link>
              </li>
              <li>
                <Link to='/search'> {t('header.search')}</Link>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </NavBars>
  )
}

export default NavBar
