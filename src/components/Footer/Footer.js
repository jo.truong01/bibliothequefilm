import React, { Component } from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { useTranslation } from 'react-i18next'

const Footer = () => {
  const { t, i18n } = useTranslation()

  return (
    <FooterContainer>
      <span style={{ marginLeft: '15%', fontSize: '1.125rem' }}>
        Questions? <Link>Call 1-234-567-8910</Link>
      </span>
      <div className='footer-columns'>
        <ul>
          <li>
            <Link>{t('footer.faq')}</Link>
          </li>
          <li>
            <Link>{t('footer.investorrelations')}</Link>
          </li>
          <li>
            <Link>{t('footer.waystowatch')}</Link>
          </li>
          <li>
            <Link>{t('footer.corporateinformation')}</Link>
          </li>
          <li>
            <Link>{t('footer.netflixoriginals')}</Link>
          </li>
        </ul>

        <ul>
          <li>
            <Link>{t('footer.help')}</Link>
          </li>
          <li>
            <Link>{t('footer.jobs')}</Link>
          </li>
          <li>
            <Link>{t('footer.termsofuse')}</Link>
          </li>
          <li>
            <Link>{t('footer.contactus')}</Link>
          </li>
        </ul>

        <ul>
          <li>
            <Link>{t('footer.account')}</Link>
          </li>
          <li>
            <Link>{t('footer.redeemgiftcards')}</Link>
          </li>
          <li>
            <Link>{t('footer.privacy')}</Link>
          </li>
          <li>
            <Link>{t('footer.speedtest')}</Link>
          </li>
        </ul>

        <ul>
          <li>
            <Link>{t('footer.mediacenter')}</Link>
          </li>
          <li>
            <Link>{t('footer.buygiftcards')}</Link>
          </li>
          <li>
            <Link>{t('footer.cookiepreferences')}</Link>
          </li>
          <li>
            <Link>{t('footer.legalnotices')}</Link>
          </li>
        </ul>
      </div>
    </FooterContainer>
  )
}

export default Footer

const FooterContainer = styled.footer`
  padding-top: 3rem;
  color: #999;

  .footer-columns {
    width: 70%;
    margin: 1rem auto 0;
    font-size: 0.9rem;
    overflow: auto;
    display: grid;
    grid-template-columns: repeat(4, 1fr);
  }

  ul li {
    list-style: none;
    line-height: 2.5;
  }

  a {
    color: #999;
  }

  a:hover {
    text-decoration: underline;
    cursor: pointer;
  }
`
