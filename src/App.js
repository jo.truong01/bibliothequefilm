import React, { useEffect } from 'react'
import { Provider } from 'react-redux'
import './App.css'
import Routes from './config/router'

import { PersistGate } from 'redux-persist/integration/react'
import { persistor, store } from './config/store'

import { useDarkMode } from './styles/useDarkMode'
import { GlobalStyles, lightTheme, darkTheme } from './styles/globalStyles'
import styled, { ThemeProvider } from 'styled-components'

const Container = styled.div`
  max-width: 50%;
  margin: 8rem auto 0;
`

function App() {
  const [theme, toggleTheme] = useDarkMode()
  const themeMode = theme === 'light' ? lightTheme : darkTheme

  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <PersistGate loading={null} persistor={persistor}>
          <GlobalStyles></GlobalStyles>
          <Routes></Routes>
        </PersistGate>
      </ThemeProvider>
    </Provider>
  )
}

export default App
