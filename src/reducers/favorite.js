import update from 'react-addons-update'
import { ADD_FAVORITE, REMOVE_FAVORITE } from '../actions/favorite'

const initialState = { movieFavorite: [] }

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case ADD_FAVORITE:
      return {
        ...state,
        movieFavorite: [...state.movieFavorite, action.payload.movie]
      }
    case REMOVE_FAVORITE:
      return {
        ...state,
        movieFavorite: state.movieFavorite.filter(
          item => item !== action.payload.movie
        )
      }
    default:
      return state
  }
}
