import update from 'react-addons-update'
import { LIST_MOVIES } from '../actions/movies'

const initialState = { listMovies: [] }

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case LIST_MOVIES: {
      return { ...state, listMovies: action.payload }
    }
    default:
      return state
  }
}
