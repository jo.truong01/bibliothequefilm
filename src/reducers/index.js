import { combineReducers } from 'redux'
import movies from './movies'
import favorite from './favorite'
import library from './library'

export default combineReducers({
  movies,
  favorite,
  library
})
