import update from 'react-addons-update'
import {
  ADD_LIBRARY,
  REMOVE_LIBRARY,
  ADD_SEEN,
  REMOVE_SEEN
} from '../actions/library'

const initialState = {
  movieLibrary: [],
  movieSeen: []
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case ADD_LIBRARY:
      return {
        ...state,
        movieLibrary: [...state.movieLibrary, action.payload.movie]
      }
    case REMOVE_LIBRARY:
      return {
        ...state,
        movieLibrary: state.movieLibrary.filter(
          item => item.id !== action.payload.movie
        )
      }
    case ADD_SEEN:
      return {
        ...state,
        movieSeen: [...state.movieSeen, action.payload.movie]
      }
    case REMOVE_SEEN:
      return {
        ...state,
        movieSeen: state.movieSeen.filter(
          item => item.id !== action.payload.movie
        )
      }
    default:
      return state
  }
}
